package zw.ac.nust.tverify.empoyee;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import zw.ac.nust.tverify.company.CompanyRepo;
import zw.ac.nust.tverify.exceptions.ResourceAlreadyExists;
import zw.ac.nust.tverify.exceptions.ResourceNotFoundException;
import zw.ac.nust.tverify.model.dto.EmployeeDto;
import zw.ac.nust.tverify.model.dto.Response;
import zw.ac.nust.tverify.model.entities.Company;
import zw.ac.nust.tverify.model.entities.Employee;
import zw.ac.nust.tverify.model.enums.Gender;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class ManageEmployeeImpl implements ManageEmployee {

    private final EmployeeRepo employeeRepo;
    private final CompanyRepo companyRepo;

    @Override
    public Response saveSingleEmployee(EmployeeDto employeeDto) {
        requireNonNull(employeeDto, "Request cannot be null");
        requireNonNull(employeeDto.getEmployeeID(), "Employee ID cannot be null");
        requireNonNull(employeeDto.getName(), "Employee name cannot be null");
        requireNonNull(employeeDto.getGender(), "Employee gender cannot be null");
        requireNonNull(employeeDto.getDepartment(), "Employee department cannot be null");
        requireNonNull(employeeDto.getRole(), "Employee role cannot be null");

        var company = companyRepo.findById(employeeDto.getCompanyId())
                .orElseThrow(()-> new ResourceNotFoundException("No company with that id number was found."));

        var employeeExists = employeeRepo.findByEmployeeID(employeeDto.getEmployeeID()).isPresent();

        if (employeeExists)
            throw new ResourceAlreadyExists("Employee with this particular employee id already exists.");

        var employee = Employee.builder()
                .role(employeeDto.getRole())
                .duties(employeeDto.getDuties())
                .employeeID(employeeDto.getEmployeeID())
                .dateStarted(employeeDto.getDateStarted())
                .gender(employeeDto.getGender())
                .name(employeeDto.getName())
                .department(employeeDto.getDepartment())
                .company(company)
                .build();

        employeeRepo.save(employee);
        company.setNumberOfEmployees(company.getNumberOfEmployees() + 1);
        companyRepo.save(company);

        return Response.builder()
                .response("Employee was saved successfully")
                .build();
    }

    @Override
    public Response updateSingleEmployee(EmployeeDto employeeDto) {

        var employee = employeeRepo.findByEmployeeIDAndCompany_Id(employeeDto.getEmployeeID(), employeeDto.getCompanyId())
                .orElseThrow(()-> new ResourceNotFoundException("Employee to update was not found. " +
                        "Make sure the employee ID and the company ID are correct"));

        if (employeeDto.getName() != null) {
            employee.setName(employeeDto.getName());
        }
        if (employeeDto.getDateLeft() != null) {
            employee.setDateLeft(employeeDto.getDateLeft());
        }
        if (employeeDto.getDepartment() != null) {
            employee.setDepartment(employeeDto.getDepartment());
        }
        if (employeeDto.getDuties() != null) {
            employee.setDuties(employeeDto.getDuties());
        }
        if (employeeDto.getRole() != null) {
            employee.setRole(employeeDto.getRole());
        }
        employeeRepo.save(employee);
        return Response.builder()
                .response("Employee was updated successfully")
                .build();
    }

    @Override
    public Response updateMultipleEmployees(Long companyId, MultipartFile employeeCsv) throws IOException {
        requireNonNull(companyId, "Company ID is required when adding employees");
        requireNonNull(employeeCsv, "No csv file was selected");

        var company = companyRepo.findById(companyId)
                .orElseThrow(() -> new ResourceNotFoundException("Failed to upload the employees " +
                        "because no company with that particular id was found"));

        List<Employee> employeeList = new ArrayList<>();

        try (InputStream inputStream = employeeCsv.getInputStream();
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

            String line;
            while ((line = reader.readLine()) != null) {

                String[] values = line.split(",");

                DateTimeFormatter formatter =
                        DateTimeFormatter.ofPattern("dd/MM/yyyy");

                var employee = Employee.builder()
                        .employeeID(values[1].trim())
                        .name(values[0].trim())
                        .role(values[4].trim())
                        .department(values[3].trim())
                        .company(company)
                        .gender(Gender.valueOf(values[2].trim().toUpperCase()))
                        .duties(values[5].trim())
                        .dateStarted(LocalDate.parse(values[6].trim(), formatter))
                        .dateLeft(LocalDate.parse(values[7].trim(), formatter))
                        .build();

                employeeList.add(employee);
            }
        }
        employeeList.forEach(updatedEmployee -> {
            var employeeOptional = employeeRepo.findByEmployeeID(updatedEmployee.getEmployeeID());

            if (employeeOptional.isPresent()) {
                var employee = employeeOptional.get();
                if (updatedEmployee.getName() != null) {
                    employee.setName(updatedEmployee.getName());
                }
                if (updatedEmployee.getDateLeft() != null) {
                    employee.setDateLeft(updatedEmployee.getDateLeft());
                }
                if (updatedEmployee.getDepartment() != null) {
                    employee.setDepartment(updatedEmployee.getDepartment());
                }
                if (updatedEmployee.getDuties() != null) {
                    employee.setDuties(updatedEmployee.getDuties());
                }
                if (updatedEmployee.getRole() != null) {
                    employee.setRole(updatedEmployee.getRole());
                }
                employeeRepo.save(employee);
            }
        });
        return Response.builder()
                .response("Employees were saved successfully")
                .build();
    }

    @Override
    public Response saveMultipleEmployees(Long id, MultipartFile multipartFile) throws IOException {
        requireNonNull(id, "Company ID is required when adding employees");
        requireNonNull(multipartFile, "No csv file was selected");

        var company = companyRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Failed to upload the employees " +
                        "because no company with that particular id was found"));
        List<Employee> employeeList = new ArrayList<>();

        try (InputStream inputStream = multipartFile.getInputStream();
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

            String line;
            while ((line = reader.readLine()) != null) {

                String[] values = line.split(",");

                DateTimeFormatter formatter =
                        DateTimeFormatter.ofPattern("dd/MM/yyyy");

                var employee = Employee.builder()
                        .employeeID(values[1].trim())
                        .name(values[0].trim())
                        .role(values[0].trim())
                        .department(values[0].trim())
                        .company(company)
                        .gender(Gender.valueOf(values[0].trim().toUpperCase()))
                        .duties(values[0].trim())
                        .dateStarted(LocalDate.parse(values[1].trim(), formatter))
                        .dateLeft(LocalDate.parse(values[1].trim(), formatter))
                        .build();

                employeeList.add(employee);
            }
        }
        employeeList.forEach(employee -> {
            var employeeExists = employeeRepo.findByEmployeeID(employee.getEmployeeID()).isPresent();
            if (!employeeExists) {
                employeeRepo.save(employee);
                company.setNumberOfEmployees(company.getNumberOfEmployees() + 1);
                companyRepo.save(company);
            }

        });
        return Response.builder()
                .response("Employees were saved successfully")
                .build();
    }

    @Override
    public List<Employee> findByEmployeeName(String name) {
        return employeeRepo.findByName(name);
    }

    @Override
    public Page<Employee> findAll(int page, int pageSize, String sortBy) {
        Pageable paging = page > 0 ? PageRequest.of(page, pageSize, Sort.by(sortBy).descending()) : Pageable.unpaged();
        return employeeRepo.findAll(paging);
    }

    @Override
    public Page<Employee> getCompanyEmployees(Long companyId, int page, int pageSize, String sortBy) {
        Pageable paging = page > 0 ? PageRequest.of(page, pageSize, Sort.by(sortBy).descending()) : Pageable.unpaged();
        return employeeRepo.findByCompany_Id(companyId, paging);
    }

    @Override
    public Optional<Employee> findByEmployeeId(String employeeId) {
        var employee = employeeRepo.findByEmployeeID(employeeId)
                .orElseThrow(()-> new ResourceNotFoundException("Employee with that employee was not found"));
        return Optional.of(employee);
    }
}
