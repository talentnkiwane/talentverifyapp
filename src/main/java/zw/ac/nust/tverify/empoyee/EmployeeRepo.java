package zw.ac.nust.tverify.empoyee;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import zw.ac.nust.tverify.model.entities.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {

    List<Employee> findByName(String name);

    Page<Employee> findByCompany_Id(Long companyId, Pageable pageable);

    Optional<Employee> findByEmployeeID(String employeeId);

    Page<Employee> findByCompany_RegistrationNumber(String registrationNumber, Pageable pageable);

    Optional<Employee> findByEmployeeIDAndCompany_Id(String employeeId, Long id);

}
