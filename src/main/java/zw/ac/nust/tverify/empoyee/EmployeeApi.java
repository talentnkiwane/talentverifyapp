package zw.ac.nust.tverify.empoyee;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import zw.ac.nust.tverify.company.CompanyService;
import zw.ac.nust.tverify.exceptions.ResourceNotFoundException;
import zw.ac.nust.tverify.model.dto.EmployeeDto;
import zw.ac.nust.tverify.model.dto.Response;
import zw.ac.nust.tverify.model.entities.Employee;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/employee")
public class EmployeeApi {

    private final ManageEmployee employeeService;
    private final CompanyService companyService;

    @PostMapping(value = "/add-employee")
    @Operation(summary = "upload a single employee")
    ResponseEntity<Response> addEmployee(@RequestBody EmployeeDto employeeDto ) {
        var response = employeeService.saveSingleEmployee(employeeDto);
        return ResponseEntity.ok(response);
    }

    @PostMapping(value = "/add-employees", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "upload multiple employees")
    ResponseEntity<Response> addEmployees(@RequestParam("companyId") Long companyId,
                                          @RequestParam("employeeCsv") MultipartFile employeeCsv) throws IOException {
        var response = employeeService.saveMultipleEmployees(companyId, employeeCsv);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/update-employee")
    @Operation(summary = "update a single employee's details")
    ResponseEntity<Response> updateEmployee(@RequestBody EmployeeDto employeeDto) {
        var response = employeeService.updateSingleEmployee(employeeDto);
        return ResponseEntity.ok(response);
    }

    @PutMapping(value = "/update-employees", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "update multiple employee details")
    ResponseEntity<Response> updateBulkEmployee(@RequestParam("companyId") Long companyId,
                                                @RequestParam("employeeCsv") MultipartFile employeeCsv) throws IOException {
        var response = employeeService.updateMultipleEmployees(companyId, employeeCsv);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/get-by-id/{employeeId}")
    @Operation(summary = "Search for an employee by their employee id")
    ResponseEntity<Employee> findByEmployeeId(@PathVariable String employeeId) {
        var employee = employeeService.findByEmployeeId(employeeId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee with this employee ID was not found."));
        return ResponseEntity.ok(employee);
    }

    @GetMapping("/get-by-name/{name}")
    @Operation(summary = "Search for an employee by their employee id")
    ResponseEntity<?> findEmployeeByName(@PathVariable String name) {
        var employeeList = employeeService.findByEmployeeName(name);
        return ResponseEntity.ok(employeeList);
    }

    @GetMapping("/list-all/")
    @Operation(summary = "List all employees in the system")
    ResponseEntity<?> getAllEmployees(@RequestParam int page,
                                             @RequestParam int pageSize,
                                             @RequestParam String sortBy) {
        var allEmployees = employeeService.findAll(page, pageSize, sortBy);
                return ResponseEntity.ok(allEmployees);
    }

    @GetMapping("/list-by-company/{companyId}")
    @Operation(summary = "List all employees for a particular company")
    ResponseEntity<?> getAllEmployeesByCompany(@PathVariable Long companyId,
                                               @RequestParam int page,
                                               @RequestParam int pageSize,
                                               @RequestParam String sortBy) {
        var companyEmployees = employeeService.getCompanyEmployees(companyId, page, pageSize, sortBy);
        return ResponseEntity.ok(companyEmployees);
    }

}
