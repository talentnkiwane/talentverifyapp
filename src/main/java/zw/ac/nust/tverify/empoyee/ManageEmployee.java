package zw.ac.nust.tverify.empoyee;

import org.aspectj.weaver.ResolvedPointcutDefinition;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;
import zw.ac.nust.tverify.model.dto.EmployeeDto;
import zw.ac.nust.tverify.model.dto.Response;
import zw.ac.nust.tverify.model.entities.Employee;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface ManageEmployee {

    Response saveSingleEmployee(EmployeeDto employeeDto);

    Response saveMultipleEmployees(Long companyId, MultipartFile file) throws IOException;

    Response updateSingleEmployee(EmployeeDto employeeDto);

    Response updateMultipleEmployees(Long companyId, MultipartFile employeeCsv) throws IOException;

    List<Employee> findByEmployeeName(String name);

    Page<Employee> findAll(int page, int pageSize, String sortBy);

    Page<Employee> getCompanyEmployees(Long companyId, int page, int pageSize, String sortBy);

    Optional<Employee> findByEmployeeId(String employeeId);

}
