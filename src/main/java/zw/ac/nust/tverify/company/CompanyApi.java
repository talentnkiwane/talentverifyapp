package zw.ac.nust.tverify.company;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import zw.ac.nust.tverify.exceptions.ResourceNotFoundException;
import zw.ac.nust.tverify.model.dto.CompanyDetailsDTO;
import zw.ac.nust.tverify.model.dto.Response;
import zw.ac.nust.tverify.model.entities.Company;
import zw.ac.nust.tverify.model.entities.Employee;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/company")
@Slf4j
public class CompanyApi {

    private final CompanyService companyService;

    @PostMapping(value = "/save-with-employees", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "save company with employees")
    public ResponseEntity<Response> addCompanyWithEmployees(@RequestBody CompanyDetailsDTO companyDetailsDTO,
                                               @RequestParam("employeeCsv") MultipartFile employeeCsv) throws IOException {
        var response = companyService.saveCompanyWithEmployees(companyDetailsDTO, employeeCsv);
        return ResponseEntity.ok(response);
    }

    @PostMapping(value = "/save-without-employees")
    @Operation(summary = "save company without employees")
    public ResponseEntity<Response> addCompanyWithoutEmployees(@RequestBody CompanyDetailsDTO companyDetailsDTO) {
        var response = companyService.saveCompanyWithoutEmployees(companyDetailsDTO);
        return ResponseEntity.ok(response);
    }

    @PutMapping(value = "/update")
    @Operation(summary = "update the company details")
    public ResponseEntity<Response> updateCompanyDetails(@RequestBody CompanyDetailsDTO companyDetailsDTO) {
        var response = companyService.updateCompanyDetails(companyDetailsDTO);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/list-employees/{registrationNumber}")
    @Operation(summary = "list all employees for a particular company")
    public List<Employee> getCompanyEmployees(@PathVariable String registrationNumber) {
        return companyService.findAllCompanyEmployees(registrationNumber);
    }

    @GetMapping("/list-companies")
    @Operation(summary = "list all companies saved in the system")
    public Page<Company> getCompanies(@RequestParam int page,
                                      @RequestParam int pageSize,
                                      @RequestParam String sortBy) {
        return companyService.findAllCompanies(page, pageSize, sortBy);
    }

    @GetMapping("/get-by-name")
    @Operation(summary = "Search for a company by its name")
    public ResponseEntity<Company> getCompanyByName(@RequestParam String name) {
        return ResponseEntity.ok(companyService.findByName(name)
                .orElseThrow(()-> new ResourceNotFoundException("Name did not match any company in the system")));
    }

    @GetMapping("/get-by-reg-number")
    @Operation(summary = "Search for a company by its registration number")
    public ResponseEntity<Company> getCompanyByRegistrationNumber(@RequestParam String registrationNumber) {
        return ResponseEntity.ok(companyService.findByRegistrationNumber(registrationNumber)
                .orElseThrow(()-> new ResourceNotFoundException("Registration did not match any company in the system")));
    }

    @GetMapping("/get-by-id/{id}")
    @Operation(summary = "Search for a company by its company id")
    public ResponseEntity<Company> getCompanyByID(@PathVariable Long id) {
        return ResponseEntity.ok(companyService.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("Registration did not match any company in the system")));
    }
}
