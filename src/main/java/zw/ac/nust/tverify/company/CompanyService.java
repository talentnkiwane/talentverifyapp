package zw.ac.nust.tverify.company;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;
import zw.ac.nust.tverify.model.dto.CompanyDetailsDTO;
import zw.ac.nust.tverify.model.dto.Response;
import zw.ac.nust.tverify.model.entities.Company;
import zw.ac.nust.tverify.model.entities.Employee;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface CompanyService {

    Response saveCompanyWithEmployees(CompanyDetailsDTO companyDetailsDTO, MultipartFile csvFile) throws IOException;

    Response saveCompanyWithoutEmployees(CompanyDetailsDTO companyDetailsDTO);

    Response updateCompanyDetails(CompanyDetailsDTO companyDetailsDTO);

    Optional<Company> findByName(String companyName);

    Optional<Company> findByRegistrationNumber(String registrationNumber);

    Page<Company> findAllCompanies(int page, int pageSize, String sortBy);

    List<Employee> findAllCompanyEmployees(String registrationNumber);

    Optional<Employee> findCompanyEmployee(String registrationNumber, String employeeId);

    Optional<Company> findById(Long id);

}
