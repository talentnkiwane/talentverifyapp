package zw.ac.nust.tverify.company;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import zw.ac.nust.tverify.empoyee.ManageEmployee;
import zw.ac.nust.tverify.exceptions.ResourceNotFoundException;
import zw.ac.nust.tverify.model.dto.CompanyDetailsDTO;
import zw.ac.nust.tverify.model.dto.Response;
import zw.ac.nust.tverify.model.entities.Company;
import zw.ac.nust.tverify.model.entities.Employee;
import zw.ac.nust.tverify.model.enums.Gender;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.requireNonNull;
import static zw.ac.nust.tverify.commons.KeyGenerator.generateCompanyId;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class CompanyServiceImpl implements CompanyService{

    private final ManageEmployee employeeService;
    private final CompanyRepo companyRepo;

    @Override
    public Response saveCompanyWithEmployees(CompanyDetailsDTO companyDetailsDTO, MultipartFile file) throws IOException {
        requireNonNull(companyDetailsDTO.getName(), "Company name can not be null");
        requireNonNull(companyDetailsDTO.getRegistrationNumber(), "Company registration number can not be null");
        requireNonNull(companyDetailsDTO.getRegistrationDate(), "Company registration date can not be null");

        if (file != null) {
            List<Employee> employeeList = new ArrayList<>();

            try (InputStream inputStream = file.getInputStream();
                 BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

                String line;
                while ((line = reader.readLine()) != null) {

                    String[] values = line.split(",");

                    DateTimeFormatter formatter =
                            DateTimeFormatter.ofPattern("dd/MM/yyyy");

                    var employee = Employee.builder()
                            .employeeID(values[1].trim())
                            .name(values[0].trim())
                            .role(values[4].trim())
                            .department(values[3].trim())
                            //must i put company or it maps automatically
                            .gender(Gender.valueOf(values[2].trim().toUpperCase()))
                            .duties(values[5].trim())
                            .dateStarted(LocalDate.parse(values[6].trim(), formatter))
                            .dateLeft(LocalDate.parse(values[7].trim(), formatter))
                            .build();
                    employeeList.add(employee);
                }
                var company = Company.builder()
                        .companyId(generateCompanyId())
                        .address(companyDetailsDTO.getAddress())
                        .name(companyDetailsDTO.getName())
                        .employees(employeeList)
                        .contactPerson(companyDetailsDTO.getContactPerson())
                        .contactPhone(companyDetailsDTO.getContactPhone())
                        .emailAddress(companyDetailsDTO.getEmailAddress())
                        .registrationDate(companyDetailsDTO.getRegistrationDate())
                        .registrationNumber(companyDetailsDTO.getRegistrationNumber())
                        .numberOfEmployees(employeeList.size())
                        //.departmentList()
                        .build();
                companyRepo.save(company);
                return Response.builder()
                        .response("Company profile has been created successfully")
                        .build();
            } catch (Exception exception) {
                return Response.builder()
                        .response("Failed to save company profile")
                        .build();
            }
        }else {
            var company = Company.builder()
                    .companyId(generateCompanyId())
                    .address(companyDetailsDTO.getAddress())
                    .name(companyDetailsDTO.getName())
                    .contactPerson(companyDetailsDTO.getContactPerson())
                    .contactPhone(companyDetailsDTO.getContactPhone())
                    .emailAddress(companyDetailsDTO.getEmailAddress())
                    .registrationDate(companyDetailsDTO.getRegistrationDate())
                    .registrationNumber(companyDetailsDTO.getRegistrationNumber())
                    .numberOfEmployees(Integer.valueOf("0"))
                    .departmentList(companyDetailsDTO.getDepartmentList())
                    .build();
            companyRepo.save(company);
            return Response.builder()
                    .response("Company profile has been created successfully")
                    .build();
        }
    }

    @Override
    public Response saveCompanyWithoutEmployees(CompanyDetailsDTO companyDetailsDTO) {
        var company = Company.builder()
                .companyId(generateCompanyId())
                .address(companyDetailsDTO.getAddress())
                .name(companyDetailsDTO.getName())
                .contactPerson(companyDetailsDTO.getContactPerson())
                .contactPhone(companyDetailsDTO.getContactPhone())
                .emailAddress(companyDetailsDTO.getEmailAddress())
                .registrationDate(companyDetailsDTO.getRegistrationDate())
                .registrationNumber(companyDetailsDTO.getRegistrationNumber())
                .numberOfEmployees(Integer.valueOf("0"))
                .departmentList(companyDetailsDTO.getDepartmentList())
                .build();
        companyRepo.save(company);
        return Response.builder()
                .response("Company profile has been created successfully")
                .build();
    }


    @Override
    public Response updateCompanyDetails(CompanyDetailsDTO companyDetailsDTO) {
        var company = companyRepo.findByRegistrationNumber(companyDetailsDTO.getRegistrationNumber())
                .orElseThrow(() -> new ResourceNotFoundException(
                        "The company you are trying to update does not exist"));

        if (companyDetailsDTO.getName() != null) {
            company.setName(companyDetailsDTO.getName());
        }
        if (companyDetailsDTO.getAddress() != null) {
            company.setAddress(companyDetailsDTO.getAddress());
        }
        if (companyDetailsDTO.getEmailAddress() != null) {
            company.setEmailAddress(companyDetailsDTO.getEmailAddress());
        }
        if (companyDetailsDTO.getContactPerson() != null) {
            company.setContactPerson(companyDetailsDTO.getContactPerson());
        }
        if (companyDetailsDTO.getContactPhone() != null) {
            company.setContactPhone(companyDetailsDTO.getContactPhone());
        }

        companyRepo.save(company);

        return Response.builder()
                .response("Company details updated successfully")
                .build();
    }

    @Override
    public Optional<Company> findByName(String companyName) {
        var company = companyRepo.findByName(companyName)
                .orElseThrow(()-> new ResourceNotFoundException("Company with that registration number does not exist"));
        return Optional.of(company);
    }

    @Override
    public Optional<Company> findByRegistrationNumber(String registrationNumber) {
        var company = companyRepo.findByRegistrationNumber(registrationNumber)
                .orElseThrow(()-> new ResourceNotFoundException("Company with that registration number does not exist"));
        return Optional.of(company);
    }

    @Override
    public Page<Company> findAllCompanies(int page, int pageSize, String sortBy) {
        Pageable paging = page > 0 ? PageRequest.of(page, pageSize, Sort.by(sortBy).descending()) : Pageable.unpaged();
        return companyRepo.findAll(paging);
    }

    @Override
    public List<Employee> findAllCompanyEmployees(String registrationNumber) {
        var company = findByRegistrationNumber(registrationNumber)
                .orElseThrow(()->new ResourceNotFoundException("Company with that reg number does not exist"));
        return company.getEmployees();
    }

    @Override
    public Optional<Employee> findCompanyEmployee(String registrationNumber, String employeeId) {
        var employeesList = findAllCompanyEmployees(registrationNumber);
        var employeeOptional = employeesList.stream()
                .filter(employee -> employee.getEmployeeID().equalsIgnoreCase(employeeId))
                .findFirst();

        if (employeeOptional.isEmpty())
            throw new ResourceNotFoundException("No such employee was found");
        return employeeOptional;
    }

    @Override
    public Optional<Company> findById(Long id) {
        return Optional.empty();
    }
}
