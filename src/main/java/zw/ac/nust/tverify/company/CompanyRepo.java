package zw.ac.nust.tverify.company;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import zw.ac.nust.tverify.model.entities.Company;
import zw.ac.nust.tverify.model.entities.Employee;

import java.util.Optional;

public interface CompanyRepo extends JpaRepository<Company, Long> {

    Optional<Company> findByCompanyId(String companyId);

    Optional<Company> findByRegistrationNumber(String registrationNumber);

    Optional<Company> findByName(String companyName);
}
