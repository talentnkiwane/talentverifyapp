package zw.ac.nust.tverify.exceptions;

public class TransactionNotFoundException extends RuntimeException{

    public TransactionNotFoundException(String message){
        super(message);
    }
}
