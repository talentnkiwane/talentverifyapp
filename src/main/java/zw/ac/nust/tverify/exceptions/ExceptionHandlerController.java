package zw.ac.nust.tverify.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice(annotations = {RestController.class})
@Slf4j
public class ExceptionHandlerController {

    @ExceptionHandler(TransactionNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody
    Error invalidRequest(TransactionNotFoundException e) {
        log.info("Validation error: {}", e.getMessage());
        return Error.of(4, e.getMessage());
    }

    @ExceptionHandler(FailedToProcessTransactionException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody
    Error resourceAlreadyExists(FailedToProcessTransactionException e) {
        log.info("Validation error: {}", e.getMessage());
        return Error.of(4, e.getMessage());
    }

    @ExceptionHandler(InvalidPhoneNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody
    Error invalidPhoneNumber(InvalidPhoneNumberException e) {
        log.info("Validation error: {}", e.getMessage());
        return Error.of(4, e.getMessage());
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody
    Error resourceNotFound(ResourceNotFoundException e) {
        log.info("Validation error: {}", e.getMessage());
        return Error.of(4, e.getMessage());
    }
}
