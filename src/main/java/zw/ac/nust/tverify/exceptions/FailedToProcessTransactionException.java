package zw.ac.nust.tverify.exceptions;

public class FailedToProcessTransactionException extends  RuntimeException{
    public FailedToProcessTransactionException(String message){
        super(message);
    }
}
