package zw.ac.nust.tverify.util;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
class MobileFormatServiceImpl implements MobileFormatService {
    @Override
    public String formatMobileNumber(String phoneNumber) {
        phoneNumber = phoneNumber.trim();
        phoneNumber = phoneNumber.replaceAll("/D+", "");
        if (phoneNumber.matches("^263\\d+$")) {
            phoneNumber =  phoneNumber.substring(3);
        }
        if (phoneNumber.matches("^0263\\d+$")) {
            phoneNumber =  phoneNumber.substring(4);
        }
        if (phoneNumber.matches("^00263\\d+$")) {
            phoneNumber = phoneNumber.substring(5);
        }
        if (phoneNumber.matches("^0\\d+$")) {
            phoneNumber = phoneNumber.substring(1);
        }

        return phoneNumber;
    }

    /*This method accepts number in local mode e.g 0783016050 and changes to 263783017060
    to be used after the {@link #formatMobileNumber(String phoneNumber)}*/

    @Override
    public String formatToInternational(String phoneNumber) {
        phoneNumber = formatMobileNumber(phoneNumber);
        phoneNumber = "263" + phoneNumber;
        return phoneNumber;
    }
}
