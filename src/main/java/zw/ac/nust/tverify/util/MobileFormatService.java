package zw.ac.nust.tverify.util;

public interface MobileFormatService {
    public String formatMobileNumber(String phoneNumber);
    String formatToInternational(String phoneNumber);
}
