package zw.ac.nust.tverify.commons;

import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import zw.ac.nust.tverify.exceptions.BusinessException;
import zw.ac.nust.tverify.model.entities.Employee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
@Data
public class Util {

    public static String generateEmployeeID() {
        return String.format("TnNsMs%s", generateRandom(12));
    }

    public static long generateNewPassword() {
        return generateRandom(6);
    }

    private static long generateRandom(int length) {
        Random random = new Random();
        char[] digits = new char[length];
        digits[0] = (char) (random.nextInt(9) + '1');
        for (int i = 1; i < length; i++) {
            digits[i] = (char) (random.nextInt(10) + '0');
        }
        return Long.parseLong(new String(digits));
    }

    public static String determineAirtimeType(String receivingNumber) {
        if (receivingNumber.matches("^26371\\d{7}$") ||
                receivingNumber.matches("^+26371\\d{7}$") ||
                receivingNumber.matches("^071\\d{7}$")) {
            return "Netone";
        }
        if (
                receivingNumber.matches("^26377\\d{7}$") ||
                        receivingNumber.matches("^+26377\\d{7}$") ||
                        receivingNumber.matches("^077\\d{7}$") ||
                        receivingNumber.matches("^26378\\d{7}$") ||
                        receivingNumber.matches("^+26378\\d{7}$") ||
                        receivingNumber.matches("^078\\d{7}$")
        ) {
            return "Econet";
        }
        throw new BusinessException("Invalid airtime type for entered number");
    }
}
