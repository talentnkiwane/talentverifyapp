package zw.ac.nust.tverify.commons;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
//class for the logic of the class eg generating uniqueids
public class KeyGenerator {

    public static String generateEmployeeID() {
        int length = 6;
        boolean useLetters = true;
        boolean useNumbers = true;
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }

    public static String generateCompanyId() {
        int length = 6;
        boolean useLetters = true;
        boolean useNumbers = false;
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }

    //get md5 of string
  /*  public String getHash(String password) {
        StringBuilder myHash = new StringBuilder();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] digest = md.digest();
            myHash.append(DatatypeConverter
                    .printHexBinary(digest).toUpperCase());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return myHash.toString();
    }

   */


}
