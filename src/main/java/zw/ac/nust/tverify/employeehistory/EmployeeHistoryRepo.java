package zw.ac.nust.tverify.employeehistory;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.ac.nust.tverify.model.entities.EmployeeHistory;

public interface EmployeeHistoryRepo extends JpaRepository<EmployeeHistory, Long> {
}
