package zw.ac.nust.tverify.model.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Company extends BaseEntity{

    private String name;
    private String companyId;
    private LocalDate registrationDate;
    private String registrationNumber;
    private String address;
    private Integer numberOfEmployees;
    private String contactPerson;
    private String contactPhone;
    private String emailAddress;
    private List<String> departmentList;
    @OneToMany(mappedBy = "company")
    private List<Employee> employees;

}
