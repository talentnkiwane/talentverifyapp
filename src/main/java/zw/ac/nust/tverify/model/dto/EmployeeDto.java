package zw.ac.nust.tverify.model.dto;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import zw.ac.nust.tverify.model.entities.Company;
import zw.ac.nust.tverify.model.entities.EmployeeHistory;
import zw.ac.nust.tverify.model.enums.Gender;

import java.time.LocalDate;
import java.util.List;

@Data
public class EmployeeDto {

    private String name;
    private String employeeID;
    private String department;
    private Gender gender;
    private Long companyId;
    private String role;
    private LocalDate dateStarted;
    private LocalDate dateLeft;
    private String duties;

}
