package zw.ac.nust.tverify.model.enums;

public enum Gender {

    MALE,
    FEMALE

}
