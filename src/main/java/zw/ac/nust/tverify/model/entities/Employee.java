package zw.ac.nust.tverify.model.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import zw.ac.nust.tverify.model.enums.Gender;

import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Employee extends BaseEntity{

    private String name;
    private String employeeID;
    private String department;

    @Enumerated(EnumType.STRING)
    private Gender gender;

//    @ManyToOne
//    @JoinColumn(name = "department_id")
//    private Department department;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id")
    private Company company;

    private String role;
    private LocalDate dateStarted;
    private LocalDate dateLeft;
    private String duties;

    @OneToMany(mappedBy = "employee")
    private List<EmployeeHistory> employeeHistory;



}
