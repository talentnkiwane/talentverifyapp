package zw.ac.nust.tverify.model.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

import java.time.LocalDate;

@Data
@Entity
public class EmployeeHistory extends BaseEntity{

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

    private String role;
    private LocalDate dateStarted;
    private LocalDate dateLeft;
    private String duties;
    private String department;

}
