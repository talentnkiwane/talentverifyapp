package zw.ac.nust.tverify.model.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class CompanyDetailsDTO {

    private String name;
    private LocalDate registrationDate;
    private String registrationNumber;
    private String address;
    private String contactPerson;
    private String contactPhone;
    private String emailAddress;
    private List<String> departmentList;

}
